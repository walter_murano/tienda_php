<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'product name_two',
            'price' => 7,
            'stock' => 5,
            'description' => 'descripcion del producto',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/pelota.jpg')),
        ]);
        //
    }
}
